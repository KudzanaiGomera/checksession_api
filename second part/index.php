<?php

// for Debug
error_reporting(E_ALL & ~E_NOTICE);
ini_set('display_errors', 1);

require __DIR__.'/../vendor/autoload.php';
use \Firebase\JWT\JWT;
 
require_once('../inc/config.php');
require_once('../inc/db.inc.php');
require_once('../inc/func.inc.php');


$req_channel  = filter_input(INPUT_GET, 'channel', FILTER_SANITIZE_STRING);
$req_storeID  = filter_input(INPUT_GET, 'storeid', FILTER_SANITIZE_STRING);
$req_summary  = filter_input(INPUT_GET, 'summary', FILTER_SANITIZE_STRING);

if(empty($req_summary)) {
    $req_summary = 0;
} else {
    $req_summary = 1;
}



if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    doCors();
    /*
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET,POST,OPTIONS');
    header('Access-Control-Allow-Headers: Content-Type, Authorization');
    */
} else {

    // do auth
    doCors();
    //$authid = authVerify();


    $headers = getallheaders();


         if (array_key_exists('Authorization', $headers)) {
             $authheader = $headers['Authorization'];
             $authheader_array = explode(' ', $authheader);
             $jwt = trim($authheader_array[1]);

             //$decoded = JWT::decode($jwt, $key, array('HS256'));
             //print_r($decoded);
             //$decoded_array = (array) $decoded;
            try {
                 $token = JWT::decode($jwt, SECRET_KEY, array('HS256'));
                 $token_array = (array) $token;
            } catch (Exception $e) {
                    //$data['AUTHERR'] = $e->getMessage();
                    http_response_code(401);
                    $json = array( 'AccountID' => 0, 'Auth' => 0, 'Error' => $e->getMessage());  
            } 

            // DEV: Verify token against current user/pass (pass change, etc)
            $authid = $token->AccountID;
            $u_auth = getAccountUserPassByID($authid,$mysqli);

            $v_token = array();
            $v_token['AccountID'] = $authid;
            $v_token['AccountTS'] = $u_auth['LastUpdateTS'];        
            $v_token['exp']      = $token->exp;
            $v_token['iat']      = $token->iat;
            $v_token['Auth']     = $token->Auth;
            $validate_token = JWT::encode($v_token, SECRET_KEY);


            if(strcmp($validate_token,$jwt) !== 0) {
                    // TOKEN MISMATCH
                    header('Cache-control: private, max-age=0, no-cache');
                    header('Expires: 0');
                    header('Content-Type: application/json');
                    http_response_code(401);
                    $json = array( 'AccountID' => 0, 'Auth' => 0, 'Error' => 'INVALID-TOKEN');
                    echo json_encode($json);
                    mysqli_close($mysqli);
exit();

            }

             
             $token_str = print_r($token_array,1);
             if ($token_array['exp'] >= time()) {
                 //loggedin
                 $authid =  $token->AccountID;
             } else {
                 //http_response_code(401);
                 $json = array( 'AccountID' => 0, 'Auth' => 0, 'Error' => 'expired');
                 echo json_encode($json);
                 return false;
                 mysqli_close($mysqli);
exit();
             }
         } else {
                 http_response_code(401);
                 $json = array( 'AccountID' => 0, 'Auth' => 0, 'Error' => 'NO-AUTHHEADER');
                 echo json_encode($json);
                 mysqli_close($mysqli);
exit();
         }

    
    if(!$authid) {
        http_response_code(403);
        $json = array(  'AccountID' => 0, 'Auth' => 0, 'Error' => 'NO-AUTHID');
        echo json_encode($json);
        mysqli_close($mysqli);
exit();
    } else {

        $userAccount     = getAccountByID($authid,$mysqli); 
       
        //$linkedSuppliers = getAllowedClientIDByAccountID($authid,$mysqli);
        $linkedSuppliers = getAllowedClientIDByAccountID($authid,$mysqli);        
        
           
        $data['DataResult']['UserAccount']     = $userAccount;
        $data['DataResult']['LinkedSuppliers'] = $linkedSuppliers;
        $data['DataResult']['LinkedSuppliersCount'] = count($linkedSuppliers); 
        
       // activityLog($AccountID , $UserAction, $ActionData, $UA, $gps, $mysqli);
        
        
        // Catch Empty Active Supplier
        if(empty($userAccount['ActiveSupplier'])) {            
            updateActiveSupplier($authid,$linkedSuppliers[0]['SupplierID'], $mysqli);
        }

    }

    header('Cache-control: private, max-age=0, no-cache');
    header('Expires: 0');
    header('Content-Type: application/json');
    echo json_encode($data, TRUE);

}




mysqli_close($mysqli);
