<?php

// displaying errors faced
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// call the database file
include('includes/db.inc.php');

//response
$response = array();

// select the data from the table checkinSession
$sql_query = "SELECT sessionID, storeID, channelCode FROM checkinSession WHERE outTS = '0000-00-00 00:00:00'";

$sql_data = mysqli_query($con, $sql_query);

// fetching data as an associative array
if($sql_data){
    $i = 0; 
    while($sql_data_arr = mysqli_fetch_assoc($sql_data)){
        $response[$i]['sessionID'] = $sql_data_arr['sessionID'];
        $response[$i]['storeID'] = $sql_data_arr['storeID'];
        $response[$i]['channelCode'] = $sql_data_arr['channelCode'];
        $i++;
    };

    // OUTPUT DATA (JSON)
    header("Content-Type: application/json");
    echo json_encode($response, JSON_PRETTY_PRINT);
}
?>